module.exports = {
  theme: {
    colors: {
      gray: {
        "100": "#F9F9F9",
        "200": "#F0F5F0",
        "300": "#F8FEF8"
      },
      black: "#1E1E1E",
      white: "#FFFFFF"
    },
    fontSize: {
      "10": "1rem",
      "12": "1.2rem",
      "14": "1.4rem",
      "16": "1.6rem",
      "26": "2.6rem",
      "36": "3.6rem"
    },
    spacing: {
      "0": "0",
      "6": "0.6rem",
      "10": "1rem",
      "20": "2rem",
      "24": "2rem",
      "28": "2.8rem",
      "30": "3rem",
      "60": "6rem"
    },
    boxShadow: {
      default: "0px 0.5px 5px rgba(0, 0, 0, 0.25)"
    },
    extend: {
      height: {
        "1": "1px"
      },
      minHeight: {
        "200": "20rem"
      },
      width: {
        "65p": "65%"
      }
    }
  },
  variants: {},
  plugins: [
    function({ addUtilities, config, e }) {
      const betweenChildren = {};

      for (const [key, value] of Object.entries(config("theme.spacing"))) {
        betweenChildren[
          `.${e(`between-children-${key}`)} > *:not(:last-child)`
        ] = {
          marginRight: value
        };
        betweenChildren[
          `.${e(`v-between-children-${key}`)} > *:not(:last-child)`
        ] = {
          marginBottom: value
        };
      }

      addUtilities(betweenChildren);
    }
  ]
};
